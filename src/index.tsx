import React from "react";
import ReactDOM from "react-dom/client";
import App from "./app";
import "./index.css";

const domContainer = document.querySelector("#react");
const root = ReactDOM.createRoot(domContainer as any);
root.render(<App />);
