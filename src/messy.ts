const user = {
  hasRole: function (cap: string) {
    return true;
  },
  id: 0,
};

function canView(scope: string, ownerId: number) {
  if (user.hasRole("admin")) {
    return true;
  } else {
    switch (scope) {
      case "public":
        return true;
        break;
      case "private":
        if (user.id === ownerId) {
          return true;
        }
        break;
      default:
        return false;
    }
    return false;
  }
}

export default canView;
