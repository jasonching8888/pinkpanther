import React from "react";
import Game from "./components/game";

import ImageArrived from "./asset/resource/PinkPanther_arrived_1.png";
import ImageDanceWith from "./asset/resource/PinkPanther_danceWith_1.png";
import ImageParty from "./asset/resource/PinkPanther_party_1.png";
import ImageShop from "./asset/resource/PinkPanther_shop_1.png";

function App() {
  return (
    <div
      style={{ backgroundColor: "#eda3c0" }}
      className="bg-black h-screen overflow-y-scroll snap-y"
    >
      <div className="h-screen snap-start sticky flex items-center">
        <img src={ImageArrived} />
      </div>
      <div className="h-screen snap-start sticky flex items-center">
        <img src={ImageParty} />
      </div>
      <div className="h-screen snap-start sticky flex items-center">
        <img src={ImageShop} />
      </div>
      <div className="h-screen snap-start sticky flex items-center">
        <img src={ImageDanceWith} />
      </div>
      <div className="h-screen snap-start sticky flex items-center">
        <Game />
      </div>
    </div>
  );
}

export default App;
