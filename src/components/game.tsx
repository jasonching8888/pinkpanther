import ImageBadge from "../asset/resource/badge.png";
import ImagePlay from "../asset/resource/PinkPanther_play_1.png";
import { useMachine } from "@xstate/react";
import Cookies from "js-cookie";
import React, { useState, useEffect } from "react";
import { gameMachine } from "../machines/gameMachine";
import { useForm } from "react-hook-form";
import { fillForm } from "../utils/form.utils";

function canPlayGame() {
  if (Cookies.get("lastPlayTime") === undefined) return true;

  const lastPlayTime = Number.parseInt(Cookies.get("lastPlayTime"));
  const durationInMs = Date.now() - lastPlayTime;

  // 5 minutes
  return durationInMs >= 1000 * 60 * 60 * 8;
}

enum Result {
  Special = "Special",
  Gift1 = "Gift1",
  Gift2 = "Gift2",
  Gift3 = "Gift3",
}

export default function Game() {
  const [state, send] = useMachine(gameMachine);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [rst, setRst] = useState<Result>(Result.Gift3);

  useEffect(() => {
    preloadImage();
  }, []);

  function preloadImage() {
    console.log("preload");
    let image = new Image();
    image.src =
      "https://i.pinimg.com/originals/7d/b3/24/7db324ed27acf79d21e3a3132287623c.gif";

    image = new Image();
    image.src = ImageBadge;
  }

  function destroy() {
    send("INIT");
  }

  async function submit(data: any) {
    try {
      await fillForm(data);
      Cookies.set("formFilled", "yes", { expires: 7 });
      send("NEXT");
    } catch (e) {
      console.error(e);
      Cookies.set("formFilled", "yes", { expires: 7 });
      send("NEXT");
    }
  }

  const FillForm = () => {
    return (
      <>
        <h2 style={{ color: "#e73e7b" }} className="text-2xl mb-10 text-center">
          Before We Start
          <br />
          在開始之前
        </h2>
        <form className="w-full px-4" onSubmit={handleSubmit(submit)}>
          <div className="flex flex-col gap-2">
            <label className="block mb-4">
              <p className="text-center mb-2">Email Address:</p>
              <input
                className="block w-full rounded-md h-10 p-2"
                {...register("email")}
              />
            </label>
            <label className="block mb-8">
              <p className="text-center mb-2">Phone:</p>
              <input
                className="block w-full rounded-md h-10 p-2"
                {...register("phone", { pattern: /^\d{8}$/ })}
              />
            </label>
            <p className="text-xs text-white mb-8">
              Above collected data will be used in Hate Monday for promotion
              purpose.
              <br />
              以上資料將會用作Hate Monday推廣用途
            </p>
            <input
              className="bg-white text-center rounded-3xl h-12 border-gray-200 border-2 drop-shadow-md text-xl"
              type="submit"
              value="Submit"
            />
          </div>
        </form>
      </>
    );
  };

  const Expired = () => {
    return (
      <div className="text-center">
        <h1 style={{ color: "#e73e7b" }} className="text-6xl mb-10">
          Wait!
        </h1>
        <h2>You can only play once every 8 hours</h2>
      </div>
    );
  };

  const Wait = () => {
    function next() {
      if (canPlayGame()) {
        // if (Cookies.get("formFilled") === undefined) {
        send("FILLFORM");
        // } else {
        //   send("PLAYING");
        // }
      } else {
        send("EXPIRED");
      }
    }

    return (
      <div>
        <button
          onClick={() => {
            next();
          }}
          style={{ top: "46%", left: "26vw", width: "47%", height: "10%" }}
          className="absolute"
        ></button>
        <img src={ImagePlay} />
      </div>
    );
  };

  const Playing = () => {
    useEffect(() => {
      start();
    }, []);

    function loaded() {
      setTimeout(() => {
        send("NEXT");
      }, 4000);
    }

    function getResult() {
      const rand = Math.random();

      if (rand <= 0.01) return Result.Special; // 1%
      if (rand <= 0.06) return Result.Gift1; // 5%
      if (rand <= 0.4) return Result.Gift2; // 34%
      return Result.Gift3; // 60%
    }

    function start() {
      const rst = getResult();
      setRst(rst);

      Cookies.set("lastPlayTime", Date.now().toString(), { expires: 7 });
    }

    return (
      <img
        onLoad={loaded}
        src="https://i.pinimg.com/originals/7d/b3/24/7db324ed27acf79d21e3a3132287623c.gif"
      />
      // <video
      //   onEnded={ended}
      //   autoPlay
      //   src="https://www.shutterstock.com/shutterstock/videos/1081487198/preview/stock-footage-open-holiday-gift-box-with-flow-red-hearts-valentines-day-high-quality-fullhd-footage.webm"
      // />
    );
  };

  const Gift = ({ grade }: { grade: string }) => {
    return (
      <>
        <div className="text-9xl">
          <img style={{ height: 250 }} src={ImageBadge} />
        </div>
        <h1 className="text-xl" style={{ color: "#e73e7b" }}>
          Congrulations!
        </h1>

        <h2>{grade}</h2>
        <div className="mt-20 text-center">
          <p className="mb-10 text-xl">
            請不要離開版面。請到1/F 換領獎品！Please don't exit page and claim
            gift on 1/F Hate Monday booth!
          </p>
          <p className="mb-5 text-sm">
            先到先得 送完即止! First come first served.
          </p>
          <p className="mb-5 text-sm">
            Offer valid while stocks last! Hate Monday reserves the right of
            final decision to any disputes.
          </p>
          <button
            onClick={destroy}
            className="bg-white text-center mb-4 rounded-3xl h-12 border-gray-200 border-2 drop-shadow-md text-sm p-2"
          >
            Destroy It
          </button>
        </div>
      </>
    );
  };

  const ResultComponent = () => {
    switch (rst) {
      case Result.Gift1:
        return <Gift grade="Gold"></Gift>;
      case Result.Gift2:
        return <Gift grade="Silver"></Gift>;
      case Result.Gift3:
        return <Gift grade="Bronze"></Gift>;
      case Result.Special:
        return <Gift grade="Special"></Gift>;
    }
  };

  const ShowState = () => {
    if (state.matches("wait")) return <Wait />;
    if (state.matches("expired")) return <Expired />;
    if (state.matches("playing")) return <Playing />;
    if (state.matches("fillForm")) return <FillForm />;
    if (state.matches("result")) return <ResultComponent />;

    return null;
  };

  return (
    <div className="w-full h-screen snap-start sticky flex flex-col items-center justify-center">
      <ShowState />
    </div>
  );
}
