const user = {
  hasRole: function (cap: string) {
    return true;
  },
  id: 0,
};

function canView(scope: string, ownerId: number) {
  if (user.hasRole("admin")) {
    return true;
  }

  switch (scope) {
    case "public":
      return true;
    case "private":
      return user.id === ownerId;
    default:
      return false;
  }
}

function canView2(user: any, scope: string, ownerId: number) {
  if (user.hasRole("admin")) {
    return true;
  }

  switch (scope) {
    case "public":
      return true;
    case "private":
      return user.id === ownerId;
    default:
      return false;
  }
}

export default canView;
