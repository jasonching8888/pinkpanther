import { createMachine } from "xstate";

export const gameMachine = createMachine({
  id: "game",
  initial: "wait",
  predictableActionArguments: true,
  states: {
    wait: {
      on: {
        FILLFORM: { target: "fillForm" },
        PLAYING: { target: "playing" },
        EXPIRED: { target: "expired" },
      },
    },
    fillForm: {
      on: {
        NEXT: { target: "playing" },
      },
    },

    expired: {
      on: {
        RESTART: { target: "wait" },
      },
    },
    win: {
      on: {
        INIT: { target: "wait" },
      },
    },
    lose: {
      on: {
        RESTART: { target: "wait" },
      },
    },
    playing: {
      on: {
        NEXT: { target: "result" },
      },
    },
    result: {
      on: {
        INIT: { target: "wait" },
      },
    },
  },
});
