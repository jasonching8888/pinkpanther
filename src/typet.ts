// type Person = {
//   name: string;
//   age: number;
// };

// type NameOnly = {
//   name: string;
// };

function getPerson() {
  return { name: "Jonny", age: 35 };
}

function printPerson(person: { name: string }) {
  console.log("name:", person.name);
}

function start() {
  const person = getPerson();
  printPerson(person);
}
