export async function fillForm(data: { email: string; phone: string }) {
  const url =
    "https://www.hishk.com/wp-json/contact-form-7/v1/contact-forms/8479/feedback";

  const formData = new FormData();

  formData.append("_wpcf7", "8479");
  formData.append("_wpcf7_version", "5.6.4");
  formData.append("_wpcf7_locale", "en_US");
  formData.append("_wpcf7_unit_tag", "wpcf7-f8479-p8480-o1");
  formData.append("_wpcf7_container_post", "8480");
  formData.append("_wpcf7_posted_data_hash", "");
  formData.append("email", data.email);
  formData.append("phone", data.phone);

  const res = await fetch(url, {
    method: "POST",
    body: formData,
  });

  if (res.status !== 200) {
    throw new Error("Failed filling form");
  }
}
