const { merge } = require("webpack-merge");
const webpackCommon = require("./webpack.common");
const path = require("path");

module.exports = merge(webpackCommon, {
  mode: "development",
  devServer: {
    static: {
      directory: path.join(__dirname, "public"),
    },
    compress: true,
    port: 9000,
    open: true,
  },
});
